#!/bin/bash
# Script to execute 'yum update' and email the results
EMAIL_LIST="me@marcusbean.com"
OUT_FILE=/home/untitled/logs/yum_update_checker.out
APP_DIR="/home/untitled/box"

# Basic Machine Information
hostname > ${OUT_FILE}
uname -a >> ${OUT_FILE}
echo "PATH: "$PATH >> ${OUT_FILE}
cat /etc/os-release >> ${OUT_FILE}
echo -e "\n\n" >> ${OUT_FILE}

# ETC files
echo "/etc/cron.d/beantech_custom OUTPUT" >> ${OUT_FILE}
cat /etc/cron.d/beantech_custom >> ${OUT_FILE}
echo -e "\n\n" >> ${OUT_FILE}

# YUM/DNF software and package info
# Run yum update and do NOT install
echo "DNF PACKAGES AVAILABLE FOR UPDATE" >> ${OUT_FILE}
dnf update << EOF >> ${OUT_FILE}
n
EOF
echo -e "\n\n" >> ${OUT_FILE}

# Hard drive info
# Grab proc mdstat info
echo "/PROC/MDSTAT INFO" >> ${OUT_FILE}
cat /proc/mdstat >> ${OUT_FILE}
# Grab smartctl info
for device in $(ls /dev/sd* | grep -v [0-9]); do
echo "SMARTCTL INFO FOR ${device}" >> ${OUT_FILE}
/usr/sbin/smartctl -a ${device} >> ${OUT_FILE}
/usr/sbin/smartctl -l scterc ${device} >> ${OUT_FILE}
done
echo -e "\n\n" >> ${OUT_FILE}

#Network Info
echo "NETWORK MAP INFO" >> ${OUT_FILE}
nmap -sn 10.0.10.1/24 >> ${OUT_FILE}
echo -e "\n\n" >> ${OUT_FILE}

echo "REDNECK NETWORK MAP INFO" >> ${OUT_FILE}
for node in 10.0.10.{1..254}; do ping -c1 $node > /dev/null 2>&1; RC=$?; if [ $RC = 0 ]; then dns_server="10.0.10.1"; nodelookup=$(nslookup $node 10.0.10.1 | awk '{print $NF}' | tr -d '[:space:]'); if [ "${nodelookup}" == "NXDOMAIN" ]; then dns_server="192.168.1.1"; nodelookup=$(nslookup $node 192.168.1.1 | awk '{print $NF}' | tr -d '[:space:]'); if [ "${nodelookup}" == "NXDOMAIN" ]; then nodelookup="NOTFOUND"; dns_server="NOTFOUND"; fi; fi; echo $node" -> "${nodelookup}" from "${dns_server} >> ${OUT_FILE}; fi; done
echo -e "\n\n" >> ${OUT_FILE}

#Container Info
# Grab docker container info
echo "DOCKER INFO" >> ${OUT_FILE}
docker ps >> ${OUT_FILE}
echo -e "\n\n" >> ${OUT_FILE}

# Email the output
mail -s "Update checker for $(date)" ${EMAIL_LIST} < ${OUT_FILE}

# Cleanup 
#rm -f ${OUT_FILE}

exit 0
