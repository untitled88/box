#!/bin/bash
# bash script to correct default nmsg JSON output
file=$1

#echo 'Correcting '${file}

# Add square bracket to the beginning of the JSON file
#echo "Adding square bracket to the beginning of ${file}"
#sed -i 1s/^/[/ ${file}

# Add comma to the end of every line
#echo "Adding comma to every line of ${file}"
#sed -i s/}$/},/g ${file}

# Remove the added comma to the last line of the file
#echo "Removing comma from end of ${file}"
#sed -i '$ s/.$//' ${file}

# Add square bracket to the end of the JSON file
#echo "Adding square bracket to the end of ${file}"
#echo "]" >> ${file}

# Remove characters that cannot be read
echo "Removing characters that cannot be read on ${file}"
tr -cd '\11\12\15\33-\176' < ${file} > ${file}.clean
mv ${file}.clean ${file}

exit 0
