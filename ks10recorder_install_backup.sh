#!/bin/bash

fromfilesum=$(ssh ks10lodge@ks10.from-ks.com -p 8822 'sudo md5sum /root/ks10recorder_install.tar.gz' | awk '{print $1}')
tofilesum=$(sudo md5sum /sambamnt/Mason_KS10/ks10recorder_install.tar.gz | awk '{print $1}')

if grep "$(date -d '1 week ago' +%Y%m%d)" /home/untitled/logs/ks10recorder_install_backup.out; then
	rm /home/untitled/logs/ks10recorder_install_backup.out
fi

if [[ $fromfilesum != $tofilesum ]]; then 
	echo $(date +%Y%m%d)" - "$(date)" - ks10recorder_install.tar.gz has changed"
	echo -ne "Copying ks10recorder_install.tar.gz from root home to ks10lodge home|"
	ssh ks10lodge@ks10.from-ks.com -p 8822 'sudo cp /root/ks10recorder_install.tar.gz /home/ks10lodge/'
	RC=$?
	if [[ ${RC} = 0 ]]; then
		echo "[SUCCESS]"
	else
		echo "[FAILED]"
	fi
	echo -ne "Changing ownership of ks10recorder_install.tar.gz in ks10lodge home|"
	ssh ks10lodge@ks10.from-ks.com -p 8822 'sudo chown ks10lodge:ks10lodge /home/ks10lodge/ks10recorder_install.tar.gz'
	RC=$?
        if [[ ${RC} = 0 ]]; then
        	echo "[SUCCESS]"
        else
        	echo "[FAILED]"
        fi
	echo -ne "Copying ks10recorder_install.tar.gz from ks10lodge home to this node|"
	scp -q -P 8822 ks10lodge@ks10.from-ks.com:/home/ks10lodge/ks10recorder_install.tar.gz /home/untitled/
	RC=$?
        if [[ ${RC} = 0 ]]; then
        	echo "[SUCCESS]"
        else
        	echo "[FAILED]"
        fi
	echo -ne "Moving ks10recorder_install.tar.gz from untitled home to sambamnt|"
	sudo mv /home/untitled/ks10recorder_install.tar.gz /sambamnt/Mason_KS10/
	RC=$?
        if [[ ${RC} = 0 ]]; then
        	echo "[SUCCESS]"
        else
        	echo "[FAILED]"
        fi
	echo -ne "Removing ks10recorder_install.tar.gz from ks10lodge home|"
	ssh ks10lodge@ks10.from-ks.com -p 8822 'rm /home/ks10lodge/ks10recorder_install.tar.gz'
        RC=$?
        if [[ ${RC} = 0 ]]; then
        	echo "[SUCCESS]"
        else
        	echo "[FAILED]"
        fi
else
	if ! ssh ks10lodge@ks10.from-ks.com -p 8822 'sudo md5sum /root/ks10recorder_install.tar.gz' > /dev/null 2>&1; then
		echo $(date +%Y%m%d)" - "$(date)" - Unable to ssh to ks10recorder"
	else
		echo $(date +%Y%m%d)" - "$(date)" - ks10recorder_install.tar.gz has not changed"
	fi
fi

# exit
exit 0
