#!/bin/bash
date=`date "+%Y%m%d"`
old_date=`date -d "4 weeks ago" "+%Y%m%d"`
backup_dir=/sambamnt/backups/mysqlbackups
backup_file=beantech_databases_${date}.sql
old_backup_file=beantech_databases_${old_date}.sql
defaults_file=/home/untitled/.my.cnf

# Take a current backup
mysqldump --defaults-extra-file=${defaults_file} --all-databases --lock-all-tables > ${backup_dir}/${backup_file}

# Remove backups more than a month old
rm -rf ${backup_dir}/${old_backup_file}
