#!/usr/bin/env ruby
# DESCRIPTION: queries Google for the given search string
require 'uri'
require 'open-uri'
require 'rubygems'
require 'json'

class Google
        def Google::search(query)
                query = query.split(/\s/).map{|x| URI.escape(x)}.join("%20")
                out = []
                begin
                        start = 0
                        max = 10
                        while start < max
                                url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&start=#{start}&safe=off&q=#{query}"
                                doc = open(url).read
                                res = JSON.parse(doc)
                                res['responseData']['results'].each do |r|
                                        out << URI.unescape(r['url'].gsub("%20","+"))
                                end
                                start += 4
                        end
                rescue
                end
                out.uniq
        end
end

if __FILE__ == $0
        if ARGV.length == 0
                puts "Usage: #{$0} <search terms>"
                exit
        end
        puts Google.search(ARGV.join(" ")).join("\n")
end
