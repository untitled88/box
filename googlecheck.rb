#!/usr/bin/env ruby
# DESCRIPTION: checks Google's Safe Browsing Check API to determine if a given domain is listed as malicious

require 'open-uri'

class GoogleSafeBrowsingAPI
        def initialize(two_level_domains)
                @ttld = []
                File.open(two_level_domains).readlines.each do |l|
                        @ttld << l.strip
                end
        end
        def normalize(domain)
                # strip off the protocol (\w{1,20}://), the URI (/), parameters (?), port number (:), and username (.*@)
                # then split into parts via the .
                parts = domain.gsub(/^\w{1,20}:\/\//,'').gsub(/[\/\?\:].*/,'').gsub(/.*?\@/,'').split(/\./)
                # grab the last two parts of the domain
                dom = parts[-2,2].join(".")
                # if the dom is in the ttld list, then use three parts
                if @ttld.index(dom)
                        dom = parts[-3,3].join(".")
                end
                dom
        end
        def check(dom)
                dom = normalize(dom)
                page = open("http://www.google.com/safebrowsing/diagnostic?site=#{dom}/").read
                if page =~ /Site is listed as suspicious/
                        return true
                end
                false
        end
end

if __FILE__ == $0
        google = GoogleSafeBrowsingAPI.new(File.dirname($0)+"/../data/two-level-tlds")
        unless ARGV.length > 0
                $stdin.each_line do |l|
                        if google.check(l.chomp)
                                puts "LISTED: #{l}"
                        end
                end
        else
                ARGV.each do |host|
                        if google.check(host)
                                puts "LISTED: #{host}"
                        end
                end
        end
end
