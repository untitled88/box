#!/usr/bin/python
from socket import inet_aton
from struct import unpack
from pprint import pprint
import pyasn
import pygeoip
import io
import json
import os
import glob
import zipfile
import gzip
import tarfile
import shutil
from multiprocessing import Pool

def mb_change_directory(mb_dir):
	if not os.getcwd() == mb_dir:
		os.chdir(mb_dir)
		currentdir = os.getcwd()
		print 'Changed directory to: {0}'.format(currentdir)

def logmsg(message):
	print message
	os.system('echo {0} | mail -s "Project status message" me@marcusbean.com'.format(message))

def correctjsonfile(file):
	# Correct JSON formating of the JSON output file
	print 'Correcting JSON formatting on {0}'.format(file)
#	with open(file, 'r') as f:
#		file_lines = [''.join([x.strip(), ",", '\n']) for x in f.readlines()]
#	with open(file, 'w') as f:
#		f.writelines(file_lines)
#	with open(file, 'r+') as f:
#		s = f.read()
#		f.seek(0)
#		f.write("[" + s)
#		f.seek(-2, 2)
#		f.truncate()
#		f.seek(0, 2)
#		f.write("\n]\n")
	os.system("tr -cd '\11\12\15\33-\176' < {0} > {0}.clean".format(file))
	os.system("mv {0}.clean {0}".format(file))

def processnmsgfile(file):
	# Copy file from storage to working directory
	print 'Copying {0} to {1}'.format(file,testworkingdir)
	shutil.copy2(file,testworkingdir)
	# Define working file to manipulate
	working_file = testworkingdir + os.path.basename(file)
	# Un-gzip file if gzipped
	if file_type(working_file) is 'gz':
		print 'Un-gzipping {0}'.format(working_file)
		# Extract file and make it the new working_file
		working_file = extract_file(working_file)
		print 'New working file {0}'.format(working_file)

def file_type(filename):
	with open(filename) as f:
		file_start = f.read(max_len)
	for magic, filetype in magic_dict.items():
		if file_start.startswith(magic):
			return filetype
	return "no match"

def extract_file(filename):
	inF = filename
	outF = os.path.dirname(inF) + "/" + os.path.basename(inF)[:-3]
	with gzip.open(inF, 'rb') as infile:
		with open(outF, 'wb') as outfile:
			for line in infile:
					outfile.write(line)
	os.remove(inF)
	return outF

magic_dict = {
    "\x1f\x8b\x08": "gz",
    "\x42\x5a\x68": "bz2",
    "\x50\x4b\x03\x04": "zip"
    }

max_len = max(len(x) for x in magic_dict)

dirs = ['/storage/pool-c1/20*','/storage/cwg-2011/20*','/storage/cwg-2012/20*','/storage/cwg-2013/20*','/storage/cwg-2014/20*','/storage/cwg/sinkhole/sie/2015*','/storage/cwg/sinkhole/sie/2016*']
testdirs = ['/storage/pool-c1/200902']

workingdir = '/home/mabean2/nmsgfiles/'
testworkingdir = '/home/mabean2/testnmsgfiles/'

searchhttp = 'http'
searchch80 = 'ch80'

# Initial GeoIP Country Database download
mb_change_directory('/home/mabean2/geoipcountry/')
os.system('wget -O /home/mabean2/geoipcountry/GeoIP-106_{0}.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=106&date={0}&suffix=tar.gz&license_key=fAYKVYPxzreH"'.format('20090201'))
tar = tarfile.open('/home/mabean2/geoipcountry/' + str(os.listdir('/home/mabean2/geoipcountry/')).strip('[]').strip('\''))
tar.extractall()
tar.close()
os.remove(str(glob.glob('/home/mabean2/geoipcountry/GeoIP-106_*.tar.gz')).strip('[]').strip('\''))
geoipcountrydb = str(glob.glob('/home/mabean2/geoipcountry/GeoIP-106_*/*.dat')).strip('[]').strip('\'')
print 'Initial GeoIP Country Database: {0}'.format(geoipcountrydb)

# Initial GeoIP ISP Database download
mb_change_directory('/home/mabean2/geoipisp/')
os.system('wget -O /home/mabean2/geoipisp/GeoIP-121_{0}.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=121&date={0}&suffix=tar.gz&license_key=fAYKVYPxzreH"'.format('20090201'))
tar = tarfile.open('/home/mabean2/geoipisp/' + str(os.listdir('/home/mabean2/geoipisp/')).strip('[]').strip('\''))
tar.extractall()
tar.close()
os.remove(str(glob.glob('/home/mabean2/geoipisp/GeoIP-121_*.tar.gz')).strip('[]').strip('\''))
geoipispdb = str(glob.glob('/home/mabean2/geoipisp/GeoIP-121_*/*.dat')).strip('[]').strip('\'')
print 'Initial GeoIP ISP Database: {0}'.format(geoipispdb)

#for monthdir in dirs:
for monthdir in testdirs:
#	daydir = monthdir + '/*'
#	daydir = monthdir + '/06'
	daydir = monthdir + '/27'
	month = monthdir.split('/')[3]
	for d in glob.glob(daydir):
		day = daydir.split('/')[4]
		ymddate = month + day
		file = d + '/*'
		print
		print ymddate
		#########################################################
		# Download required asn database for the day being ran	#
		#########################################################
		mb_change_directory('/home/mabean2/asndb')
		asndatabase = '/home/mabean2/asndb/ipasn_{0}.dat'.format(ymddate)
		os.system("echo {0} > dates_to_download".format(ymddate))
		os.system("pyasn_util_download.py --dates-from-file dates_to_download")
		rib = glob.glob('/home/mabean2/asndb/rib*')
		print rib
		os.system('pyasn_util_convert.py --single {0} ipasn_{1}.dat'.format(str(rib).strip('[]'),ymddate))
		os.remove(str(rib).strip('[]').strip('\''))
		logmsg('ASN Database: {0}'.format(asndatabase))
		#########################################################
                # Download required GeoIP Country Databse		#
                #########################################################
		mb_change_directory('/home/mabean2/geoipcountry')
		command = os.system('wget -O GeoIP-106_{0}.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=106&date={0}&suffix=tar.gz&license_key=fAYKVYPxzreH"'.format(ymddate))
		if command == 0:
			shutil.rmtree(str(glob.glob('/home/mabean2/geoipcountry/GeoIP-106_*/')).strip('[]').strip('\''))
			tar = tarfile.open(str(os.listdir('./')).strip('[]').strip('\''))
			tar.extractall()
			tar.close()
			os.remove(str(glob.glob('GeoIP-106_*.tar.gz')).strip('[]').strip('\''))
			geoipcountrydb = str(glob.glob('/home/mabean2/geoipcountry/GeoIP-106_*/*.dat')).strip('[]').strip('\'')
		if not command == 0:
			os.remove(str(glob.glob('GeoIP-106_*.tar.gz')).strip('[]').strip('\''))
		logmsg('GeoIP Country Database: {0}'.format(geoipcountrydb))
		#########################################################
                # Download required GeoIP ISP Database			#
                #########################################################
		mb_change_directory('/home/mabean2/geoipisp')
                command = os.system('wget -O GeoIP-121_{0}.tar.gz "https://download.maxmind.com/app/geoip_download?edition_id=121&date={0}&suffix=tar.gz&license_key=fAYKVYPxzreH"'.format(ymddate))
                if command == 0:
			shutil.rmtree(str(glob.glob('/home/mabean2/geoipisp/GeoIP-121_*/')).strip('[]').strip('\''))
                        tar = tarfile.open(str(os.listdir('./')).strip('[]').strip('\''))
                        tar.extractall()
                        tar.close()
                        os.remove(str(glob.glob('GeoIP-121_*.tar.gz')).strip('[]').strip('\''))
                        geoipispdb = str(glob.glob('/home/mabean2/geoipisp/GeoIP-121_*/*.dat')).strip('[]').strip('\'')
		if not command == 0:
			os.remove(str(glob.glob('GeoIP-121_*.tar.gz')).strip('[]').strip('\''))
                logmsg('GeoIP ISP Database: {0}'.format(geoipispdb))
		#########################################################
		# Copy over the days log files and process them		#
		#########################################################
		logmsg("Start of copying files for files in {0}".format(d))
		mb_change_directory(testworkingdir)
		if __name__ == '__main__':
			pool = Pool(processes=4)
			ronames = glob.glob(file)
			pool.map(processnmsgfile, ronames)
			pool.close()
			pool.join()
		for f in glob.glob(file):
                        # Define working file to manipulate
                        if f.endswith('.gz'):
				working_file = testworkingdir + os.path.basename(f)[:-3]
			else:
				working_file = testworkingdir + os.path.basename(f)
			print 'Start processing file: {0}'.format(working_file)
			# Determine date and hour based on nmsg feed
			if searchch80 in working_file:
				scriptname = os.path.basename(working_file)
				date = scriptname.split(".")[4]
				hour = scriptname.split(".")[5][0:2] + "00"
				print 'ch80 in filename, date: %s   hour: %s' % (date,hour)
			if searchhttp in working_file:
				scriptname = os.path.basename(working_file)
				date = scriptname.split(".")[1]
				hour = scriptname.split(".")[2]
				print 'http in filename, date: %s   hour: %s' % (date,hour)
			# Issue nmsgtool command to output to JSON file
			jsonfile = testworkingdir + date + '.' + hour + '.out'
			print 'Running nmsgtool to {0}'.format(jsonfile)
			os.system('nmsgtool -r {0} -J - >> {1}'.format(working_file,jsonfile))
			print 'Removing file %s' % working_file
			os.remove(working_file)
		jsonfiles = testworkingdir + '/*'
		for jsonfile in glob.glob(jsonfiles):
			correctjsonfile(jsonfile)
		#########################################################
                # Start processing and disecting the files		#
                #########################################################
		# Remove ASN database
		os.remove(asndatabase)

# Final remove GeoIP Country Database
shutil.rmtree(str(glob.glob('/home/mabean2/geoipcountry/GeoIP-106_*/')).strip('[]').strip('\''))

# Final remove GeoIP ISP Database
shutil.rmtree(str(glob.glob('/home/mabean2/geoipisp/GeoIP-121_*/')).strip('[]').strip('\''))


#asndb = pyasn.pyasn(asndatabase)
#gi = pygeoip.GeoIP(geoipdb)

#with open('/home/mabean2/nmsgfiles/http.20090206.1100.1233918000.054525625.nmsg.out', 'r') as data_file:
#    data = json.load(data_file)
##with open('/home/mabean2/clean-file', 'r') as data_file:
##    data = json.load(data_file)
##with open('/home/mabean2/test.out', 'r') as data_file:
##    data = json.load(data_file)

#for row in data:
#    message = row['message']
#    print(row['time'] + ',' + ' ' + message['srcip'])
#    print message['srcip']
#    print message['request']

#with open('/home/mabean2/data.txt', 'w') as outfile:
#    json.dump(data, outfile)

#def ip2long(ip_addr):
#    ip_packed = inet_aton(ip_addr)
#    ip = unpack("!L", ip_packed)[0]
#    return ip

#for line in open('/home/mabean2/20090206.out'):
#    second_field = line.rstrip('\n').split(',')[4].split('"')[3]
#    asnumber = asndb.lookup(second_field)
#    countryCode = gi.country_code_by_addr(second_field)
#    converted_ip = ip2long(second_field)
#    if countryCode == 'US':
#        print '\n',second_field,',',converted_ip,',',countryCode,asnumber

#    print "\n" + second_field
#    print converted_ip
#    print countryCode
#    print asnumber


